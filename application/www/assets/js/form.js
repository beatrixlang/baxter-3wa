'use strict'


$('.form-control').on('keyup', function() {
	var input = $(this);

	if (input.val() !== '') {
		input.addClass('is-valid');
	} else {
		input.removeClass('is-valid');
	}
});
