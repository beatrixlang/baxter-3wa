'use strict';

var mymap = L.map('mapid').setView([48.8532898, 2.331016399999953], 15);

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
   maxZoom: 15,
   attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
     '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
     'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
   id: 'mapbox.streets'
}).addTo(mymap);

L.marker([48.8532898, 2.331016399999953]).addTo(mymap)
   .bindPopup("<b>Baxter Galerie</b><br>15 Rue du Dragon<br>75006 Paris<br>France").openPopup();
