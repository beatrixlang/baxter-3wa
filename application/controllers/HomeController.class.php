<?php

class HomeController
{
    public function httpGetMethod(Http $http)
    {
        // Récupération de toutes les galeries.
        $galerieModel = new GalerieModel();
        $galeries     = $galerieModel->listAll();

        return [
            'active' => 'galerie',
            'galeries' => $galeries,
        ];
    }
}
