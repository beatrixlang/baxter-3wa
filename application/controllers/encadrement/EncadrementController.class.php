<?php

class EncadrementController
{
	public function httpGetMethod(Http $http, array $urlParams)
	{
		//Récupération des informations sur encadrement
		$encadrementModel = new EncadrementModel();
		$encadrements     = $encadrementModel->listAll();

		return array(
    		'active'       => 'encadrement',
    		'encadrements' => $encadrements,
        );
	}
}
