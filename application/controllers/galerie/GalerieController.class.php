<?php


class GalerieController
{
	public function httpGetMethod(Http $http, array $urlParams)
	{
        // Validation du parametre GET.
        if (array_key_exists('id', $urlParams) == true) {
            if (ctype_digit($urlParams['id']) == true) {

				// Récupération des informations sur la galerie.
                $galerieModel = new GalerieModel();
				$galerie      = $galerieModel->findById($urlParams['id']);

				// Récupération des informations sur les oeuvres de la galerie.
                $oeuvreModel = new OeuvreModel();
				$oeuvres     = $oeuvreModel->findByGalerie($urlParams['id']);

				return array(
            		'active' 	=> 'galerie',
		            'galerie'	=> $galerie,
		            'oeuvres'	=> $oeuvres,
		        );
            }
        }

        // En cas d'erreur, redirection vers la page d'accueil.
        $http->redirectTo('/');
	}
}
