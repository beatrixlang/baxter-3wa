<?php

class ContactController
{
	public function httpGetMethod(Http $http, array $urlParams)
	{
		return array(
            'active' => 'contact',
        );
	}

	public function httpPostMethod(Http $http, array $postParams)
	{
		$ok = false;
		$status = false;

        if (isset($postParams['submit'])) {
          	$ok = true;
            $subject = htmlspecialchars($postParams['subject']);
            $message = htmlspecialchars($postParams['message']);
            $firstname = htmlspecialchars($postParams['firstname']);
            $lastname = htmlspecialchars($postParams['lastname']);
            $email = htmlspecialchars($postParams['email']);


	        if (!isset($subject)
	        	|| !is_string($subject)
	        	|| trim($subject) == "") {

	            $ok = false;
	        }
	        if (!isset($message)
	        	|| !is_string($message)
	        	|| trim($message) == "") {

	            $ok = false;
	        }
	        if (!isset($firstname)
	        	|| !is_string($firstname)
	        	|| trim($firstname) == "") {

	            $ok = false;
	        }
	        if (!isset($lastname)
	        	|| !is_string($lastname)
	        	|| trim($lastname) == "") {

	            $ok = false;
	        }
	        if (!isset($email)
	        	|| !is_string($email)
	        	|| trim($email) == "") {

	            $ok = false;
	        }

	        if ($ok == true) {
	        	$contactModel = new ContactModel();
				$contactModel->create(
					$subject,
		            $message,
		            $firstname,
		            $lastname,
		            $email
		        );

		        $this->sendMail($subject, $message);

		        $status = 'Votre message a été envoyé.';
	        }
        }

		return array(
            'active' => 'contact',
            'status' => $status,
            'ok' => $ok,
        );
	}

	private function sendMail($subject, $message)
	{
		$mailTo = 'baxterprints@hotmail.com';

		$headers = 'From: "Baxter Galerie"<'.$mailTo.'>'."\n";
		$headers .= 'Content-Type: text/plain; charset="UTF-8"'."\n";
		$headers .= 'Content-Transfer-Encoding: 8bit';

		mail($mailTo, $subject, $message, $headers);
	}
}
