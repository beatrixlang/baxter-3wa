<?php

class OeuvreModel
{
    public function findByGalerie($galerieId)
    {
        $database = new Database();

        $sql = 'SELECT
                    id,
                    lien_img,
                    description,
                    artiste,
                    annee,
                    titre,
                    galerie_id
                FROM Oeuvre
                WHERE galerie_id = :galerie_id';

        // Récupération des données spécifiées.
        return $database->query($sql, array(
            'galerie_id' => $galerieId,
        ));
    }
}
