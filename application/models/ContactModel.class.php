<?php

class ContactModel
{

    public function create($subject, $message, $firstname, $lastname, $email)
    {
        $sql = 'INSERT INTO Contact (
            Subject,
            Message,
            FirstName,
            LastName,
            Email,
            createdAt
        ) VALUES (?, ?, ?, ?, ?, ?)';

        // Insertion d'une demande de contact dans la base de données.
        $database = new Database();
        $database->executeSql($sql, array(
            $subject,
            $message,
            $firstname,
            $lastname,
            $email,
            date('Y-m-d'),
        ));
    }



}
