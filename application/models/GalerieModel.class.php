<?php

class GalerieModel
{
    public function findById($galerieId)
    {
        $database = new Database();

        $sql = 'SELECT *
                FROM Galerie
                WHERE Id = :galerie_id';

        // Récupération de la galerie spécifié.
        return $database->queryOne($sql, array(
            'galerie_id' => $galerieId
        ));
    }

    public function listAll()
    {
        $database = new Database();

        $sql = 'SELECT * FROM Galerie';

        // Récupération de toutes les galeries.
        return $database->query($sql);
    }
}
